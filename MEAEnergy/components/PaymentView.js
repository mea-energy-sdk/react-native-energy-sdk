import React, { useState } from 'react';
import {
  View,
  Image,
  Button,
  Platform,
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import numeral from 'numeral';
// import { MaterialCommunityIcons } from '@expo/vector-icons';
import Barcode from 'react-native-barcode-builder';
// import Barcode from 'react-native-barcode-expo';
import QRCode from 'react-native-qrcode-svg';
import PoweredBy from './common/PoweredBy';
import OriText from './common/OriText';

const Touchable = (Platform.OS === 'android'
  ? TouchableNativeFeedback
  : TouchableOpacity);

const capsuleButtonStyle = {
  active: {
    backgroundColor: '#FFEADB',
    width: 113,
    height: 37,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 19,
    flexDirection: 'row',
    marginHorizontal: 4,
  },
  inactive: {
    backgroundColor: '#ffffff',
    width: 113,
    height: 37,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 19,
    flexDirection: 'row',
    marginHorizontal: 4,
  },
  icon: {
    width: 20,
    height: 20,
  }
};

const capsuleButtonText = {
  barcode: {
    text: 'Barcode',
  },
  qr: { text: 'QR Code' },
};

const CapsuleButton = ({ active = false, type = 'barcode', onPress }) => {
  const fontColor = active ? '#FF7D1C' : '#707070';
  const isBarcode = type === 'barcode';
  return (
    <Touchable onPress={onPress} disabled={active}>
      <View
        style={
          capsuleButtonStyle[active ? 'active' : 'inactive']
        }
      >
        {
          isBarcode && <Image
            style={[capsuleButtonStyle.icon, { tintColor: fontColor }]}
            source={require('../assets/images/barcode.png')}
          />
        }
        {
          !isBarcode && <Image
            style={[capsuleButtonStyle.icon, { tintColor: fontColor }]}
            source={require('../assets/images/qrcode.png')}
          />
        }
        <OriText fontSize={18} color={fontColor} style={{ marginLeft: 5 }}>
          {capsuleButtonText[type].text}
        </OriText>
      </View>
    </Touchable>
  );
};

const HintTextBlock = ({ hintText, containerStyle = {} }) => {
  return (
    <View style={{ padding: 30, ...containerStyle }}>
      {hintText && (
        <OriText fontSize={22} color="#FF7D1C" style={{ textAlign: 'center' }}>
          {hintText}
        </OriText>
      )}
    </View>
  );
};

const BarcodeBlock = ({
  value,
  hintText,
}) => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Barcode
        value={value}
        format="CODE128"
        width={2}
        height={100}
        textColor="#000000"
        lineColor="#000000"
        background="#ffffff"
      />
      <HintTextBlock hintText={hintText} />
    </View>
  );
};

const QrBlock = ({
  value,
  hintText,
}) => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 0,
        marginBottom: 10,
      }}
    >
      <View style={{ backgroundColor: '#ffffff', padding: 10 }}>
        <QRCode size={200} value={value.replace(/<CR>/g, '\r')} />
      </View>
      <HintTextBlock hintText={hintText} containerStyle={{ paddingTop: 0 }} />
    </View>
  );
};

const PaymentView = ({
  onClose,
  amount,
  barcode,
  qr,
  paymentType,
  theme,
  isShow = false,
}) => {
  const [selectedPage, setPageType] = useState(paymentType);

  const pageType = selectedPage || paymentType;

  return (
    <Modal style={{backgroundColor: 'white', margin: 0}} isVisible={isShow} animationInTiming={1} animationOutTiming={1}>
      <View style={{
        flex: 1,
      }}>
        <View style={{ backgroundColor: '#FF7D1C', height: 243 }} />
        <View style={{ marginTop: -200 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'flex-end',
              paddingRight: 16,
            }}
          >
            <Button title="Close" color="#ffffff" onPress={() => {
              onClose();
              setPageType('');
            }} />
          </View>
          <View style={{ alignItems: 'center' }}>
            <OriText fontSize={40} color="#FFFFFF">
              ฿ {numeral(amount).format('0,0.00')}
            </OriText>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginVertical: 20,
            }}
          >
            <CapsuleButton
              active={pageType === 'barcode'}
              type="barcode"
              onPress={() => setPageType('barcode')}
            />
            <CapsuleButton
              active={pageType === 'qr'}
              type="qr"
              onPress={() => setPageType('qr')}
            />
          </View>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginVertical: 20,
          }}>
            {pageType === 'barcode' && (
              <BarcodeBlock value={barcode.code} hintText={barcode.hint} />
            )}
            {pageType === 'qr' && <QrBlock value={qr.code} hintText={qr.hint} />}
          </View>
          <View style={{ height: 40 }}>
            <PoweredBy theme={theme} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default PaymentView;
