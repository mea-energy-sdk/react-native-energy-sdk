import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { VictoryChart, VictoryAxis, VictoryBar, VictoryLine, VictoryScatter } from 'victory-native'
import { Defs, Stop, LinearGradient } from 'react-native-svg'

import { getHistory } from './../../services'
import { windowWidth } from './utils'
import { numberWithCommas } from './../../utils'

const months = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']

export default class extends React.Component {
  state = {
    consumptionHistory: undefined,
    consumptionHistoryType: 'amount',
    dimension: {},
  }

  getConsumptionHistory() {
    const { env, apiKey, clientCode, ca, onError } = this.props
    getHistory({
      env,
      apiKey,
      clientCode,
      ca,
      type: 'all',
    })
      .then(data => {
        this.setState({
          consumptionHistory: data,
        })
      })
      .catch(error => {
        if (onError) {
          onError(error)
        }
      })
  }

  componentDidMount() {
    this.getConsumptionHistory()
  }

  render() {
    const { theme, containerBackground, chartBGColor, height, minHeight } = this.props
    const { consumptionHistory, consumptionHistoryType } = this.state
    let labels = []
    let data = []
    if (consumptionHistory) {
      consumptionHistory.forEach(item => {
        labels.push(months[item.month])
        data.push((consumptionHistoryType === 'amount' ? item.amount : item.unit) || null)
      })
    }
    return (
      <View
        style={{
          backgroundColor: containerBackground,
          alignItems: 'center',
          paddingBottom: 15,
          height: height,
          minHeight,
        }}>
        {consumptionHistory && (
          <>
            <View
              style={{
                height: 40,
                marginHorizontal: 54,
                flexDirection: 'row',
                alignItems: 'center',
                borderBottomColor: 'rgba(255, 255, 255, 0.38)',
                borderBottomWidth: 1,
              }}>
              {labels.map((item, index) => (
                <Text
                  key={index}
                  style={{
                    fontFamily: 'DBOzoneX',
                    fontSize: 22,
                    flex: 1,
                    textAlign: 'center',
                    color: theme === 'dark' ? '#FFFFFF' : 'rgba(127, 127, 127, 0.8)',
                  }}>
                  {item}
                </Text>
              ))}
            </View>
            <View
              style={{
                backgroundColor: chartBGColor,
                width: '100%',
              }}>
              <VictoryChart
                width={windowWidth}
                height={188}
                padding={{
                  top: 32,
                  left: 80,
                  right: 80,
                }}>
                <Defs>
                  <LinearGradient id="barChart" x1="0%" x2="0%" y1="100%" y2="0%">
                    <Stop offset="0%" stopColor="#c43a31" />
                    <Stop offset="100%" stopColor="#fbbb5c" />
                  </LinearGradient>
                </Defs>
                <VictoryAxis
                  style={{
                    axis: { stroke: 'transparent' },
                    ticks: { stroke: 'transparent' },
                    tickLabels: { fill: 'transparent' },
                  }}
                />
                <VictoryBar
                  style={{ data: { fill: 'url(#barChart)' } }}
                  barRatio={1}
                  data={data}
                  animate={{
                    onLoad: { duration: 1000 },
                    duration: 0,
                  }}
                />
                <VictoryLine
                  style={{
                    data: { stroke: '#c43a31' },
                  }}
                  data={data}
                  interpolation="natural"
                  animate={{
                    onLoad: { duration: 1000 },
                    duration: 0,
                  }}
                />
                <VictoryScatter
                  style={{
                    data: {
                      stroke: '#c43a31',
                      strokeWidth: 2,
                      fill: '#ffffff',
                    },
                    labels: {
                      stroke: theme === 'dark' ? '#151f28' : '#ffffff',
                      strokeWidth: 4,
                      fontSize: 12,
                    },
                  }}
                  size={5}
                  data={data}
                  labels={data.map(item => numberWithCommas(item, consumptionHistoryType === 'amount' ? 2 : 0))}
                />
                <VictoryScatter
                  style={{
                    data: {
                      stroke: '#c43a31',
                      strokeWidth: 2,
                      fill: '#ffffff',
                    },
                    labels: {
                      fill: '#c43a31',
                      fontSize: 12,
                    },
                  }}
                  size={5}
                  data={data}
                  labels={data.map(item => numberWithCommas(item, consumptionHistoryType === 'amount' ? 2 : 0))}
                />
              </VictoryChart>
              {/* <View
                style={{
                  borderTopColor: 'rgba(255, 255, 255, 0.38)',
                  borderTopWidth: 2,
                  width: this.state.dimension.width,
                  alignSelf: 'center'
                }}
              /> */}
            </View>
            <View
              style={{
                height: 36,
                borderRadius: 18,
                backgroundColor: theme === 'dark' ? '#111b24' : '#fe7e34',
                padding: 4,
                marginTop: 10,
                width: 280,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                style={{
                  height: 28,
                  borderRadius: 14,
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: consumptionHistoryType === 'unit' ? '#ffffff' : undefined,
                }}
                onPress={() => {
                  this.setState({
                    consumptionHistoryType: 'unit',
                  })
                }}>
                <Text
                  style={{
                    fontFamily: 'DBOzoneX',
                    fontSize: 22,
                    textAlign: 'center',
                    color: consumptionHistoryType === 'unit' ? (theme === 'dark' ? '#111b24' : '#fe7e34') : '#ffffff',
                  }}>
                  หน่วยไฟฟ้า
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 28,
                  borderRadius: 14,
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: consumptionHistoryType === 'amount' ? '#ffffff' : undefined,
                }}
                onPress={() => {
                  this.setState({
                    consumptionHistoryType: 'amount',
                  })
                }}>
                <Text
                  style={{
                    fontFamily: 'DBOzoneX',
                    fontSize: 22,
                    textAlign: 'center',
                    color: consumptionHistoryType === 'amount' ? (theme === 'dark' ? '#111b24' : '#fe7e34') : '#ffffff',
                  }}>
                  หน่วยเงิน (บาท)
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    )
  }
}
