import React, {Component} from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput } from 'react-native';
import Modal from 'react-native-modal';

import {cancelSendOutage} from './../../services';

class CancelSendOutage extends Component {
  state = {
    message: '',
  }
  confirm = async () => {
    const {env, apiKey, clientCode, ca} = this.props;
    const {message} = await cancelSendOutage({
      env, apiKey, clientCode, ca,
    });
    this.setState({message});
  }
  close = (requestAnnounment) => {
    const {onCancelSendOutageSuccess} = this.props;
    if (requestAnnounment && onCancelSendOutageSuccess) {
      onCancelSendOutageSuccess();
    }
    this.setState({message: ''});
    this.props.onClose();
  }
  render() {
    const {isVisible} = this.props;
    const hasMessage = !!this.state.message;
    const message = hasMessage ?
      this.state.message :
      'ยืนยันการแจ้งยกเลิกไฟฟ้าดับ';
    return (
      <Modal
        isVisible={isVisible}
      >
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={{...styles.contentTitle}}>{message}</Text>
          </View>
          <View style={styles.footer}>
            {
              hasMessage ? 
              (
                <View style={styles.footerContent}>
                  <TouchableOpacity onPress={() => this.close(true)} style={styles.button}>
                    <Text style={styles.text}>ปิด</Text>
                  </TouchableOpacity>
                </View>
              ) :
              (
                <>
                  <View style={[styles.footerContent, {borderRightWidth: 1}]}>
                    <TouchableOpacity onPress={this.close} style={styles.button}>
                      <Text style={styles.text}>ยกเลิก</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.footerContent}>
                    <TouchableOpacity onPress={this.confirm} style={styles.button}>
                      <Text style={styles.text}>ยืนยัน</Text>
                    </TouchableOpacity>
                  </View>
                </>
              )
            }
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingTop: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  input: {
    height: 56,
    borderColor: 'rgba(127, 127, 127, 0.18)',
    borderWidth: 1,
    borderRadius: 5,
    fontFamily: 'DBOzoneX',
    fontSize: 26,
    color: 'black',
    textAlign: 'center'
  },
  footer: {
    height: 56,
    flexDirection: 'row'
  },
  footerContent: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    borderTopWidth: 1,
    borderColor: 'rgba(127, 127, 127, 0.18)',
  },
  text: {
    fontFamily: 'DBOzoneX',
    fontSize: 22,
    color: '#46A6F4'
  },
  button: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentTitle: {
    fontFamily: 'DBOzoneX',
    fontSize: 24,
    marginBottom: 12,
  },
});

export default CancelSendOutage;
