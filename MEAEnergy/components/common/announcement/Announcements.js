import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native'

import Modal from './../Modal'
import Paginator from './../Paginator'
import AnnouncementItem from './AnnouncementItem'
import { getMessaging } from './../../../services'

import { windowWidth, ORANGE, defineThemeValue } from '../utils'

class Announcements extends Component {
  _isMounted = false
  state = {
    dimension: {},
    announcements: [],
    selectedAnnouncement: false,
    allAnnouncements: [],
    showAllAnnouncements: false,
    error: false,
    isLoading: true,
  }
  async fetchAnnoucements(all = false) {
    const { env, apiKey, clientCode, ca, onError } = this.props
    try {
      const announcements = await getMessaging({
        env,
        apiKey,
        clientCode,
        ca,
        limit: all ? 0 : '',
      })
      if (!all) {
        return this.setAnnouncements(announcements)
      }
      return this.setAllAnnouncement(announcements)
    } catch (error) {
      if (onError) {
        onError(error)
      }
      this.setState({ error: true })
    }
  }
  componentDidMount() {
    this._isMounted = true
    if (this._isMounted) {
      this.fetchAnnoucements(false)
      this.setState({ isLoading: false })
    }
  }

  setDimension = (dimension) => {
    this.setState({ dimension })
  }
  setAnnouncements = (announcements) => {
    this.setState({ announcements })
  }
  selectAnnouncement = (selectedAnnouncement) => {
    this.setState({ selectedAnnouncement })
  }
  setAllAnnouncement = (allAnnouncements) => {
    this.setState({ allAnnouncements })
  }
  setShowAllAnnouncements = (showAllAnnouncements) => {
    this.setState({ showAllAnnouncements })
  }
  showAllAnnouncementsModal = () => {
    this.setShowAllAnnouncements(true)
    if (!this.state.allAnnouncements.length) {
      // let page render an empty page before fetch the data
      setTimeout(() => {
        this.fetchAnnoucements(true)
      }, 50)
    }
    this.fetchAnnoucements(true)
  }
  hideAllAnnouncementsModal = () => {
    this.setShowAllAnnouncements(false)
  }
  onCancelSendOutageSuccess = () => {
    this.fetchAnnoucements()
    if (this.state.selectedAnnouncement) {
      this.selectAnnouncement(false)
    }
    if (this.state.showAllAnnouncements) {
      this.setShowAllAnnouncements(false)
    }
  }
  componentWillUnmount() {
    this.selectAnnouncement(false)
    this.setShowAllAnnouncements(false)
  }
  render() {
    const { theme, containerBackgroundColor, env, apiKey, clientCode, ca, messageBackgroundColor } = this.props
    const { selectedAnnouncement, announcements, allAnnouncements, showAllAnnouncements, dimension, error } = this.state
    // individal announcement
    const showAnnouncement = !!selectedAnnouncement
    const close = () => this.selectAnnouncement(false)
    const show = (announcement) => this.selectAnnouncement(announcement)

    const define = defineThemeValue(theme)
    const containerBackground = define('#111b24', '#f8f6f7', '#f8f6f7')
    const itemBackgroundColor = messageBackgroundColor || define('#323d47', '#ffffff', '#ffffff')
    const activeColor = define('#ffffff', '#646463', '#646463')
    const inactiveColor = define('#25282E', '#E0E0DF', '#E0E0DF')
    const headerColor = define(ORANGE, ORANGE, ORANGE)
    const detailColor = define('#ffffff', '#4b4b4b', '#4b4b4b')
    const modalHeaderColor = define('#ffffff', '#282828', '#282828')
    const noDataColor = define('#323d47', '#C9C9C9', '#C9C9C9')
    const noDataIconColor = define('#323d47', '#D7D7D7', '#D7D7D7')
    const cancelColor = define('#FFFFFF', 'rgb(211, 47, 47)', 'rgb(211, 47, 47)')
    const borderColor = define('#172029', '#EBEBEB', '#EBEBEB')

    const hasData = Boolean(announcements.length)

    const props = {
      env,
      apiKey,
      clientCode,
      ca,
      onCancelSendOutageSuccess: this.onCancelSendOutageSuccess,
      cancelColor,
    }
    return (
      <View
        style={{ ...styles.announcementContainer, backgroundColor: containerBackgroundColor, paddingBottom: 0 }}
        onLayout={(event) => {
          this.setDimension(event.nativeEvent.layout)
        }}>
        <View style={styles.announcementHeader}>
          <View style={{ flex: 1 }}>
            <Text style={{ ...styles.text, color: detailColor }}>{error ? '' : 'แจ้งเตือน'}</Text>
          </View>
          {hasData && (
            <TouchableOpacity onPress={this.showAllAnnouncementsModal} style={{ flex: 1, alignItems: 'flex-end' }}>
              <Text style={{ ...styles.text, color: ORANGE }}>ทั้งหมด</Text>
            </TouchableOpacity>
          )}
        </View>
        {hasData ? (
          <Paginator
            data={announcements}
            renderItem={({ item, index }) => (
              <AnnouncementItem
                {...dimension}
                announcements={announcements}
                index={index}
                {...item}
                itemBackgroundColor={itemBackgroundColor}
                headerColor={headerColor}
                detailColor={detailColor}
                borderColor={borderColor}
                onPress={show}
                {...props}
              />
            )}
            keyExtractor={(item) => item.messageId}
            itemWidth={dimension.width - 36}
            activeColor={activeColor}
            inactiveColor={inactiveColor}
          />
        ) : (
          !error && (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <>
                <Image
                  style={{ ...styles.noAnounmentIcon, tintColor: noDataIconColor }}
                  source={require('../../../assets/images/icn_nonoti.png')}
                />
                <Text style={{ ...styles.text, color: noDataColor }}>คุณยังไม่มีข้อความแจ้งเตือน</Text>
              </>
            </View>
          )
        )}

        <Modal
          isVisible={showAnnouncement}
          onSwipeComplete={close}
          title={'แจ้งเตือน'}
          contentStyle={{
            backgroundColor: containerBackground,
          }}
          titleStyle={{
            color: modalHeaderColor,
          }}
          showHalfBottom
          bottomSpace>
          <AnnouncementItem
            width={windowWidth}
            announcements={[]}
            index={0}
            {...selectedAnnouncement}
            itemBackgroundColor={itemBackgroundColor}
            headerColor={headerColor}
            detailColor={detailColor}
            borderColor={borderColor}
            noLimit={true}
            {...props}
          />
        </Modal>

        <Modal
          isVisible={showAllAnnouncements}
          onSwipeComplete={this.hideAllAnnouncementsModal}
          title={'รายการแจ้งเตือน'}
          contentStyle={{
            backgroundColor: containerBackground,
          }}
          titleStyle={{
            color: modalHeaderColor,
          }}
          bottomSpace>
          {allAnnouncements.map((announcement) => (
            <AnnouncementItem
              key={announcement.messageId}
              announcements={[]}
              {...announcement}
              index={0}
              width={windowWidth}
              itemBackgroundColor={itemBackgroundColor}
              headerColor={headerColor}
              detailColor={detailColor}
              borderColor={borderColor}
              noLimit={true}
              containerStyle={{ marginBottom: 10 }}
              {...props}
            />
          ))}
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: 260,
    paddingVertical: 16,
  },
  header: {
    height: 36,
    flexDirection: 'row',
    paddingHorizontal: 24,
  },
  text: {
    fontFamily: 'DBOzoneX',
    fontSize: 22,
  },
  announcementHeader: {
    height: 36,
    flexDirection: 'row',
    paddingHorizontal: 24,
  },
  announcementContainer: {
    height: 260,
    paddingVertical: 16,
  },
  noAnounmentIcon: {
    width: 67,
    height: 67,
    marginBottom: 8,
  },
})

export default Announcements
