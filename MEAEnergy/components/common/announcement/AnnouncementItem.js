import React, { useState } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import moment from 'moment'

import { announcementTypeColors, ORANGE } from '../utils'
import CancelSendOutage from './../CancelSendOutage'

const AnnouncementItem = ({
  messageType,
  workStatus,
  title,
  index,
  detail,
  messageDate,
  itemBackgroundColor,
  announcements,
  headerColor,
  detailColor,
  borderColor,
  onPress,
  noLimit = false,
  containerStyle = {},
  width = 0,
  cancelColor,
  env,
  apiKey,
  clientCode,
  ca,
  onCancelSendOutageSuccess,
}) => {
  const [showCancel, setShowCancel] = useState(false)
  const ViewContainer = onPress ? TouchableOpacity : View
  const formatedDate = moment(messageDate).add(543, 'year').format('DD/MM/YY HH:mm')
  const showCancelOutage = messageType === 'NOTIOUTAGE' && workStatus === 0
  const props = { env, apiKey, clientCode, ca, onCancelSendOutageSuccess }
  return (
    <ViewContainer
      activeOpacity={0.7}
      onPress={() => onPress(announcements[index])}
      style={{
        ...styles.item,
        borderLeftColor: announcementTypeColors[messageType],
        backgroundColor: itemBackgroundColor,
        ...(announcements.length === 1
          ? {
              marginLeft: 16,
              width: width - 32,
            }
          : index === 0 || index === announcements.length - 1
          ? {
              width: width - 52,
              ...(index === 0
                ? {
                    marginLeft: 16,
                    marginRight: 8,
                  }
                : {
                    marginLeft: 8,
                    marginRight: 16,
                  }),
            }
          : {
              marginHorizontal: 8,
              width: width - 52,
            }),
        ...(noLimit ? { width: width - 32, marginLeft: 0, marginRight: 0 } : {}),
        ...containerStyle,
      }}>
      <View style={[styles.header]}>
        <Text numberOfLines={!noLimit ? 1 : undefined} style={[styles.headerText, { color: headerColor }]}>
          {title}
        </Text>
        {showCancelOutage && (
          <TouchableOpacity
            style={[styles.cancelOuter, { borderColor: cancelColor }]}
            onPress={() => setShowCancel(true)}>
            <Text style={[styles.cancel, { color: cancelColor }]}>ยกเลิกการแจ้งเหตุ</Text>
          </TouchableOpacity>
        )}
      </View>

      <View style={[styles.content, { borderTopColor: borderColor }]}>
        <Text numberOfLines={!noLimit ? 3 : undefined} style={[styles.detailText, { color: detailColor }]}>
          {detail}
        </Text>
      </View>

      <View style={[styles.footer]}>
        <Text style={styles.dateText}>{formatedDate}</Text>
      </View>
      <CancelSendOutage {...props} isVisible={showCancel} onClose={() => setShowCancel(false)} />
    </ViewContainer>
  )
}

const styles = StyleSheet.create({
  item: {
    borderRadius: 8,
    flex: 1,
    borderLeftWidth: 10,
    height: 'auto',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  content: {
    flex: 3,
    borderTopWidth: 1,
    paddingHorizontal: 12,
    paddingVertical: 8,
  },
  footer: {
    justifyContent: 'center',
    alignSelf: 'flex-end',
    paddingRight: 16,
    paddingBottom: 6,
  },
  cancelOuter: {
    borderWidth: 1,
    paddingHorizontal: 12,
    borderRadius: 25,
    alignContent: 'center',
    justifyContent: 'center',
  },
  cancel: {
    fontFamily: 'DBOzoneX',
    fontSize: 18,
  },
  headerText: {
    fontFamily: 'DBOzoneX',
    fontSize: 24,
  },
  detailText: {
    fontFamily: 'DBOzoneX',
    fontSize: 22,
  },
  dateText: {
    fontFamily: 'DBOzoneX',
    color: '#BDBDBD',
    fontSize: 22,
  },
})

export default AnnouncementItem
