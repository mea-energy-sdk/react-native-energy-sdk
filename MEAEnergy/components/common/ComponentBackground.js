import * as React from 'react';
import {View} from 'react-native'
import {SvgXml} from 'react-native-svg';
import {defineThemeValue} from './utils';

export default props => {
    const {radius = 0, width = 0, height = 0, secondBackgroundColor} = props;
    const topPathPosition = width - (width * 0.45);
    const bottomPathPosition = width - (width * 0.15);
    const xml = `
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
        >
            <g>
                <path fill="${secondBackgroundColor}" d="M ${topPathPosition} 0 L ${width-radius} 0 a${radius},${radius} 0 0 1 ${radius},${radius} L ${width} ${height - 15} a${radius},${radius} 0 0 1 ${-radius},${radius} L ${bottomPathPosition} ${height} Z" />
            </g>
        </svg>
    `;
    return <View style={{borderRadius: 15}}>
      <SvgXml xml={xml} width="100%" height="100%" style={{borderRadius: 15}} />
    </View>;
};

//<path fill="${color}" d="M ${topPathPosition} 0 L ${width} 0 L ${width} ${height} L ${bottomPathPosition} ${height} Z" />
