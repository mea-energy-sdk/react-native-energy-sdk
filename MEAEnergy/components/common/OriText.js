import React from 'react';
import {
  Text,
} from 'react-native';

const OriText = ({
  children,
  fontSize = 14,
  fontFamily = 'DBOzoneX',
  color = '#707070  ',
  style,
}) => {
  return (
    <Text style={[style, { fontSize, fontFamily, color }]}>{children}</Text>
  );
};

export default OriText;
