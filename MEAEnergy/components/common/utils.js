import React, { useState } from 'react'
import { Dimensions, View } from 'react-native'

export const ORANGE = '#fe7e34'

export const windowWidth = Math.min(Dimensions.get('window').width, Dimensions.get('window').height)

export const announcementTypeColors = {
  SENTOUTAGE: 'rgb(145, 210, 49)',
  OUTAGE: 'rgb(161, 106, 254)',
  BILLED_ACP: 'rgb(255, 5, 138)',
  BILLED_REJ: 'rgb(255, 5, 138)',
  WARNING: 'rgb(157, 65, 76)',
  BILLING: 'rgb(142, 196, 84)',
  NOTIOUTAGE: 'rgb(211, 47, 47)',
}

// first parameter is for dark, light and mea theme correspondingly
export const defineThemeValue = theme => (...properties) => {
  const keys = {
    dark: 0,
    light: 1,
    mea: 2,
  }
  return properties[keys[theme]]
}

export const withDimension = Component => props => {
  const [dimension, setDimension] = useState({})
  return (
    <View
      onLayout={event => {
        setDimension(event.nativeEvent.layout)
      }}>
      <Component {...props} dimension={dimension} />
    </View>
  )
}
