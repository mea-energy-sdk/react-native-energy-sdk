import React, { Component } from 'react'
import Modal from 'react-native-modal'
import { StyleSheet, View, Text, TouchableOpacity, ScrollView } from 'react-native'

class MEAModal extends Component {
  state = {
    scrollOffset: null,
  }
  constructor(props) {
    super(props)

    this.scrollViewRef = React.createRef()
  }
  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    })
  }
  handleScrollTo = p => {
    if (this.scrollViewRef.current) {
      this.scrollViewRef.current.scrollTo(p)
    }
  }
  render() {
    const {
      children,
      isVisible,
      onSwipeComplete,
      title,
      contentStyle,
      titleStyle,
      showHalfBottom,
      bottomSpace,
    } = this.props
    const contentHeight = { height: showHalfBottom ? 'auto' : '87%' }
    return (
      <Modal
        isVisible={isVisible}
        onSwipeComplete={onSwipeComplete}
        swipeDirection={['down']}
        style={styles.view}
        scrollTo={this.handleScrollTo}
        scrollOffset={this.state.scrollOffset}
        scrollOffsetMax={0} // content height - ScrollView height
        propagateSwipe={true}
        onBackdropPress={onSwipeComplete}>
        <View style={[styles.content, contentHeight, contentStyle]}>
          <View style={styles.header}>
            <Text style={[styles.contentTitle, titleStyle]}>{title}</Text>
            <TouchableOpacity style={styles.closeContent} onPress={onSwipeComplete}>
              <Text style={styles.close}>x</Text>
            </TouchableOpacity>
          </View>
          <ScrollView
            ref={this.scrollViewRef}
            onScroll={this.handleOnScroll}
            scrollEventThrottle={16}
            style={styles.scrollView}>
            {children}
            {bottomSpace && <View style={{ height: 40 }} />}
          </ScrollView>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  view: {
    margin: 0,
    justifyContent: 'flex-end',
  },
  content: {
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  header: {
    borderBottomColor: 'rgba(127, 127, 127, 0.18)',
    borderBottomWidth: 1,
    width: '100%',
    paddingTop: 8,
    alignItems: 'center',
    marginTop: 0,
    marginBottom: 5,
  },
  contentTitle: {
    fontSize: 24,
    marginBottom: 12,
    fontFamily: 'DBOzoneX',
  },
  closeContent: {
    position: 'absolute',
    top: 5,
    right: 15,
  },
  close: {
    fontFamily: 'DBOzoneX',
    fontSize: 25,
    fontWeight: 'bold',
    color: 'rgb(127, 127, 127)',
  },
  scrollView: {
    paddingTop: 8,
  },
})

export default MEAModal
