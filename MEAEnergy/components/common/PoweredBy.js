import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import MEASmartLifeLogo from './MEASmartLifeLogo';

export default class PoweredBy extends Component {
    render() {
        const {theme} = this.props;
        let backgroundColor, textColor;
        switch (theme) {
            case 'dark':
                backgroundColor = '#111b24';
                textColor = '#f8f6f7';
                break;
            case 'light':
                backgroundColor = '#f8f6f7';
                textColor = '#4b4b4b';
                break;
            default:
                backgroundColor = '#f8f6f7';
                textColor = '#4b4b4b';
                break;
        }
        return (
            <View
                style={[styles.container, {backgroundColor: backgroundColor}]}>
                <Text style={[styles.text, {color: textColor}]}>
                    Powered by
                </Text>
                <MEASmartLifeLogo theme={theme} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'DBOzoneX',
        fontSize: 18,
        marginTop: 1,
        marginRight: 3,
    },
});
