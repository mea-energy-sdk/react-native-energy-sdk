import React, { Component } from 'react';
import {StyleSheet, FlatList, View, Animated, Platform} from 'react-native';
import PropTypes from 'prop-types';

const IOS = Platform.OS === 'ios';

const VIEWABILITY_CONFIG = {
    viewAreaCoveragePercentThreshold: 50,
};
const ORANGE = 'rgb(237,172,113)';
const GRAY = 'rgba(0,0,0,0.3)';

const styles = StyleSheet.create({
    animationContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 8,
    },

    indicator: {
        height: 7,
        width: 7,
        borderRadius: 2,
    },
});

class Paginator extends Component {
    state = {
        visibleElement: 0,
        scrollValue: new Animated.Value(0),
    }

    constructor(props) {
        super(props)
        this.flatlist = React.createRef();
    }

    render() {
        const {
            data,
            renderItem,
            keyExtractor,
            contentContainerStyle,
            itemWidth,
        } = this.props
        const {
            scrollValue,
        } = this.state
        return (
            <>
                <FlatList
                    onScroll={Animated.event([
                        {
                            nativeEvent: {contentOffset: {x: scrollValue}},
                        },
                    ])}
                    data={data}
                    renderItem={renderItem}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={contentContainerStyle}
                    keyExtractor={keyExtractor}
                    ref={this.flatlist}
                    getItemLayout={this.getItemLayout}
                    viewabilityConfig={VIEWABILITY_CONFIG}
                    onViewableItemsChanged={this.onViewableItemsChanged}
                    onScrollEndDrag={this.onScrollEndDrag}
                />
                {/* <View style={styles.animationContainer}>
                    <Animated.View
                        style={[
                            {
                                backgroundColor: scrollValue.interpolate({
                                    inputRange: [0, itemWidth],
                                    outputRange: [ORANGE, GRAY],
                                    extrapolate: 'clamp',
                                }),
                            },
                            styles.indicator,
                        ]}
                    />
                    <Animated.View
                        style={[
                            {
                                backgroundColor: scrollValue.interpolate({
                                    inputRange: [0, itemWidth],
                                    outputRange: [GRAY, ORANGE],
                                    extrapolate: 'clamp',
                                }),
                                marginLeft: 11,
                            },
                            styles.indicator,
                        ]}
                    />
                </View> */}
            </>
        )
    }
    getItemLayout = (data, index) => {
        const {
            itemWidth,
        } = this.props
        return {
            length: itemWidth,
            offset: itemWidth * index,
            index,
        }
    };

    onViewableItemsChanged = info => {
        const {
            visibleElement,
        } = this.state
        const {index = 0} = info.viewableItems[0];
        this.setState({
            visibleElement: index,
        })
    };

    onScrollEndDrag = e => {
        const {
            data,
        } = this.props
        let visibleElement = this.state.visibleElement
        const speed = e.nativeEvent.velocity.x;
        if (IOS) {
            if (speed > 1 && visibleElement < data.length - 1) {
                visibleElement += 1;
            }
            if (speed < -1 && visibleElement > 0) {
                visibleElement -= 1;
            }
        }
        //  needs to be tested
        if (!IOS) {
            if (speed < -1 && visibleElement < data.length - 1) {
                visibleElement += 1;
            }
            if (speed > 1 && visibleElement > 0) {
                visibleElement -= 1;
            }
        }
        this.flatlist.current.scrollToIndex({
            index: visibleElement,
            animated: true,
            viewPosition: 0.5,
        });
        this.setState({
            visibleElement,
        }, () => {
            // this.flatlist.current.scrollToIndex({
            //     index: this.state.visibleElement,
            //     animated: true,
            //     viewPosition: 0.5,
            // });
        })
    };
}

Paginator.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({})),
    renderItem: PropTypes.func,
    keyExtractor: PropTypes.func,
    contentContainerStyle: PropTypes.shape({}),
    itemWidth: PropTypes.number,
};

export default Paginator;
