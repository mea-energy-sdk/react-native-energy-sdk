import React, {Component} from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput } from 'react-native';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';

import {sendOutage} from './../../services';

const TEL_NO_KEY = 'TEL_NO';

class SendOutage extends Component {
  state = {
    telNo: '',
    message: '',
  }
  async componentDidMount() {
    const telNo = await AsyncStorage.getItem(TEL_NO_KEY);
    this.setState({telNo});
  }
  onChangeText = (telNo) => {
    if (telNo.length > 10) return;
    if (!(/^\d+$/g.test(telNo)) && telNo.length) {
      return;
    }
    this.setState({telNo});
  }
  confirm = async () => {
    const {telNo} = this.state;
    const {env, apiKey, clientCode, ca} = this.props;
    await AsyncStorage.setItem(TEL_NO_KEY, telNo);
    const {message} = await sendOutage({
      env, apiKey, clientCode, ca, telNo,
    });
    this.setState({message});
  }
  close = (isCancel) => {
    const {onSendOutageSuccess} = this.props;
    this.setState({message: ''});
    this.props.onClose();
    if (onSendOutageSuccess && !isCancel) {
      onSendOutageSuccess();
    }
  }
  render() {
    const {isVisible} = this.props;
    const hasMessage = !!this.state.message;
    const message = hasMessage ?
      this.state.message :
      'ยืนยันการแจ้งไฟฟ้าดับด้วยหมายเลขโทรศัพท์';
    return (
      <Modal
        isVisible={isVisible}
      >
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={{...styles.contentTitle}}>{message}</Text>
            {!hasMessage && <TextInput
              style={styles.input}
              keyboardType="number-pad"
              placeholder="0800000000"
              placeholderTextColor={'gray'}
              onChangeText={this.onChangeText}
              value={this.state.telNo}
            />}
          </View>
          <View style={styles.footer}>
            {
              hasMessage ? 
              (
                <View style={styles.footerContent}>
                  <TouchableOpacity onPress={() => this.close(false)} style={styles.button}>
                    <Text style={styles.text}>ปิด</Text>
                  </TouchableOpacity>
                </View>
              ) :
              (
                <>
                  <View style={[styles.footerContent, {borderRightWidth: 1}]}>
                    <TouchableOpacity onPress={() => this.close(true)} style={styles.button}>
                      <Text style={styles.text}>ยกเลิก</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.footerContent}>
                    <TouchableOpacity onPress={this.confirm} style={styles.button}>
                      <Text style={styles.text}>ยืนยัน</Text>
                    </TouchableOpacity>
                  </View>
                </>
              )
            }
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingTop: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  input: {
    height: 56,
    borderColor: 'rgba(127, 127, 127, 0.18)',
    borderWidth: 1,
    borderRadius: 5,
    fontFamily: 'DBOzoneX',
    fontSize: 26,
    color: 'black',
    textAlign: 'center'
  },
  footer: {
    height: 56,
    flexDirection: 'row'
  },
  footerContent: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    borderTopWidth: 1,
    borderColor: 'rgba(127, 127, 127, 0.18)',
  },
  text: {
    fontFamily: 'DBOzoneX',
    fontSize: 22,
    color: '#46A6F4'
  },
  button: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentTitle: {
    fontFamily: 'DBOzoneX',
    fontSize: 24,
    marginBottom: 12,
  },
});

export default SendOutage;
