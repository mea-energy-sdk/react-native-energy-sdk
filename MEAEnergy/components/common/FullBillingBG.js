import * as React from 'react';
import {SvgXml} from 'react-native-svg';

export default props => {
    const {theme} = props;
    let path1Color1, path1Color2, path2Color, path3Color, path4Color, path5Color;
    switch (theme) {
        case 'dark':
            path1Color1 = '#343e47';
            path1Color2 = '#444c57';
            path2Color = '#414954';
            path3Color = '#2c333b';
            path4Color = '#1c2024';
            path5Color = '#333d47';
            break;
        case 'light':
            path1Color1 = '#fefafb';
            path1Color2 = '#fefafb';
            path2Color = '#f4f2f3';
            path3Color = '#f8dccd';
            path4Color = '#f8dccd';
            path5Color = '#fefafb';
            break;
        default:
            path1Color1 = '#f69564';
            path1Color2 = '#f7a26a';
            path2Color = '#f7a84c';
            path3Color = '#fa7f45';
            path4Color = '#e9614b';
            path5Color = '#f58f4f';
            break;
    }
    const xml = `
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
        >
            <g>
                <linearGradient
                    id="a"
                    gradientUnits="userSpaceOnUse"
                    x1="280"
                    y1="120"
                    x2="220"
                    y2="165"
                >
                    <stop offset="0" stop-color="${path1Color1}" />
                    <stop offset="1" stop-color="${path1Color2}" />
                </linearGradient>
                <path fill="url(#a)" d="M 0 0 L 220 0 L 362 222 L 0 120 Z" />
                <path fill="${path2Color}" d="M 219 0 L 480 0 L 480 140 L 361 221 Z" />
                <path fill="${path3Color}" d="M 359 221 L 480 139 L 480 240 L 372 240 Z" />
                <path fill="${path4Color}" d="M 361 220 L 376 240 L 330 240 Z" />
                <path fill="${path5Color}" d="M 0 119 L 361 220 L 332 240 L 0 240 Z" />
            </g>
        </svg>
    `;
    return <SvgXml xml={xml} width="150%" height="100%" />;
};
