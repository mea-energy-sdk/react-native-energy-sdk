import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import moment from 'moment';

import {getBilling} from './../../services';
import {windowWidth, defineThemeValue} from './utils';
import {numberWithCommas} from './../../utils';
import SendOutage from './SendOutage';
import PaymentView from '../PaymentView';

export default class extends React.Component {
  state = {
    billing: undefined,
    showSendOutage: false,
    paymentType: '',
    error: false,
  };
  onPaymentPress = (paymentType) => {
    const {billing} = this.state;
    const isPaid = billing && billing.amount === 0;
    if (isPaid) return;
    this.setState({paymentType});
  };

  onPaymentClose = () => {
    this.setState({paymentType: ''});
  };
  getBilling() {
    const {env, apiKey, clientCode, ca, onError} = this.props;
    getBilling({
      env,
      apiKey,
      clientCode,
      ca,
      requireQRCodeHint: true,
      requireBarcodeHint: true,
      requireQRCodeString: true,
    })
      .then((data) => {
        this.setState({
          billing: data,
        });
      })
      .catch((error) => {
        if (onError) {
          onError(error);
        }
        this.setState({error: error});
      });
  }
  componentDidMount() {
    this.getBilling();
  }
  render() {
    const {
      ca,
      theme,
      env,
      apiKey,
      clientCode,
      scanBGColorProp,
      colorBottom,
      hideSendOutage,
      onSendOutageSuccess,
    } = this.props;
    const {billing, showSendOutage, paymentType, error} = this.state;
    const props = {theme, env, apiKey, clientCode, ca};
    const isPaid = billing && billing.amount === 0;
    const define = defineThemeValue(theme);
    const buttonBGColor = define('#fe7e34', '#fe7e34', 'rgba(255, 255, 255, 0.3)');
    let scanBGColor = define('rgba(192, 192, 192, 0.1)', 'rgba(192, 192, 192, 0.1)', 'rgba(0, 0, 0, 0.1)');
    scanBGColor = scanBGColorProp || scanBGColor;
    let scanTextColor = define('#ffffff', '#4b4b4b', '#ffffff');
    scanTextColor = colorBottom
      ? define('#4b4b4b', '#4b4b4b', '#4b4b4b')
      : scanTextColor;

    let scanBottonBGColor = define('#fe7e34', '#fe7e34', 'rgba(255, 255, 255, 0.3)');
    scanBottonBGColor = colorBottom
      ? define('#ffffff', '#ffffff', '#ffffff')
      : scanBottonBGColor;

    const circleButtonStyle = [
      styles.circleButton,
      colorBottom && {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
      },
    ];

    const circleButtonIconStyle = [
      styles.circleButtonIcon,
      colorBottom && {
        tintColor: '#000',
      },
    ];
    return (
      <>
        <PaymentView
          isShow={!!paymentType}
          value={ca}
          hintText={billing?.barcodeHint}
          onClose={this.onPaymentClose}
          theme={theme}
          amount={billing?.amount ?? 0}
          paymentType={paymentType}
          barcode={{
            code: ca,
            hint: billing?.barcodeHint,
          }}
          qr={{
            code: billing?.qrCodeString,
            hint: billing?.qrCodeHint,
          }}
        />
        <SendOutage
          {...props}
          isVisible={showSendOutage}
          onSendOutageSuccess={onSendOutageSuccess}
          onClose={() => this.setState({showSendOutage: false})}
        />
        <View style={styles.firstContainer}>
          <View>
            <Text
              style={[
                styles.text,
                {
                  color: theme === 'light' ? '#4b4b4b' : '#ffffff',
                },
              ]}>
              บัญชีแสดงสัญญา
            </Text>
            <Text
              style={[
                styles.text,
                styles.textCA,
                {
                  fontWeight: 'bold',
                  color: theme === 'light' ? '#fe7e34' : '#ffffff',
                },
              ]}>
              {billing && ca ? ca : '-'}
            </Text>
          </View>
          {!hideSendOutage && (
            <TouchableOpacity
              disabled={!!error}
              style={{marginLeft: 16}}
              onPress={() => this.setState({showSendOutage: true})}>
              <View
                style={[styles.roundButton, {backgroundColor: buttonBGColor}]}>
                <Text style={styles.outageText}>แจ้งไฟฟ้าดับ</Text>
              </View>
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.secondContainer}>
          <Text
            style={[
              styles.text,
              styles.textAmount,
              {
                color:
                  theme === 'light' || theme === 'dark' ? '#fe7e34' : '#ffffff',
              },
            ]}>
            <Text
              style={[
                styles.text,
                styles.textCurency,
                {
                  color:
                    theme === 'light' || theme === 'dark'
                      ? '#fe7e34'
                      : '#ffffff',
                },
              ]}>
              ฿
            </Text>{' '}
            {billing ? numberWithCommas(billing.amount, 2) : '-'}
          </Text>
          <Text
            style={[
              styles.text,
              {
                marginTop: -8,
                textAlign: 'right',
                color: theme === 'light' ? '#4b4b4b' : '#ffffff',
              },
            ]}>
            {` วันครบกำหนด ${
              billing && !isPaid && billing.dueDate
                ? moment
                    .utc(billing.dueDate, 'YYYY-MM-DD')
                    .add(543, 'year')
                    .format('DD/MM/YY')
                : '-'
            }`}
          </Text>
        </View>
        <View
          style={[
            styles.thirdContainer,
            {
              backgroundColor: scanBGColor,
            },
          ]}>
          <Text
            style={[
              styles.text,
              {
                flex: 1,
                color: scanTextColor,
              },
            ]}>
            สแกนเพื่อชำระ
          </Text>
          <TouchableOpacity
            style={{marginLeft: 16}}
            disabled={!billing || isPaid}
            onPress={() => this.onPaymentPress('barcode')}>
            <View
              style={[
                ...circleButtonStyle,
                {
                  backgroundColor: scanBottonBGColor,
                },
              ]}>
              <Image
                style={circleButtonIconStyle}
                source={require('../../assets/images/barcode.png')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{marginLeft: 16}}
            disabled={!billing || isPaid}
            onPress={() => this.onPaymentPress('qr')}>
            <View
              style={[
                ...circleButtonStyle,
                {
                  backgroundColor: scanBottonBGColor,
                },
              ]}>
              <Image
                style={circleButtonIconStyle}
                source={require('../../assets/images/qrcode.png')}
              />
            </View>
          </TouchableOpacity>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  billingContainer: {
    position: 'relative',
    height: 240,
  },
  billingBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    transform: [
      {
        translateX: -(
          (windowWidth <= 480 ? 480 - windowWidth : windowWidth - 480) / 2
        ),
      },
    ],
  },
  text: {
    color: 'white',
    fontFamily: 'DBOzoneX',
    fontSize: 22,
  },
  textCA: {
    fontWeight: 'bold',
    fontSize: 32,
  },
  textAmount: {
    fontWeight: 'bold',
    fontSize: 56,
    textAlign: 'right',
  },
  textCurency: {
    fontWeight: 'bold',
    fontSize: 40,
  },
  firstContainer: {
    flex: 1,
    padding: 24,
    paddingBottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  secondContainer: {
    flex: 2,
    padding: 24,
    paddingTop: 12,
  },
  thirdContainer: {
    flex: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 24,
  },
  circleButton: {
    width: 52,
    height: 52,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  roundButton: {
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  circleButtonIcon: {
    width: 26,
    height: 26,
  },
  outageText: {
    fontFamily: 'DBOzoneX',
    fontSize: 20,
    color: '#ffffff',
  },
});
