import React, {Component} from 'react';
import {StyleSheet, View, Text, Button} from 'react-native';
import Barcode from 'react-native-barcode-builder';

export default class BarcodeViews extends Component {
    render() {
        const {value, hintText, onClose} = this.props;
        return (
            <View style={styles.container} style={{minHeight: 480}}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        paddingRight: 16,
                    }}>
                    <Button title="Close" onPress={onClose} />
                </View>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <Barcode value={value} />
                    <View style={{padding: 24}}>
                        {hintText && (
                            <Text style={[styles.text, {textAlign: 'center'}]}>
                                {hintText}
                            </Text>
                        )}
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        fontFamily: 'DBOzoneX',
        fontSize: 22,
    },
});
