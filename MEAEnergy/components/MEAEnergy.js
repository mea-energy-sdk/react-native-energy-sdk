import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import moment from 'moment';
import BarcodeView from './BarcodeView';
import QRCodeView from './QRCodeView';
import FullBillingBG from './common/FullBillingBG';
import PoweredBy from './common/PoweredBy';
import {numberWithCommas} from '../utils';
import {
    VictoryChart,
    VictoryAxis,
    VictoryBar,
    VictoryLine,
    VictoryScatter,
} from 'victory-native';
import {Defs, Stop, LinearGradient} from 'react-native-svg';
import Paginator from './common/Paginator';

import {fetchBilling, fetchConsumptionHistory} from '../services';

const windowWidth =
    Dimensions.get('window').width < Dimensions.get('window').height
        ? Dimensions.get('window').width
        : Dimensions.get('window').height;
const months = [
    'ม.ค.',
    'ก.พ.',
    'มี.ค.',
    'เม.ย.',
    'พ.ค.',
    'มิ.ย.',
    'ก.ค.',
    'ส.ค.',
    'ก.ย.',
    'ต.ค.',
    'พ.ย.',
    'ธ.ค.',
];

export default class MEAEnergy extends Component {
    onErrorCalled = false;

    state = {
        billing: undefined,
        consumptionHistory: undefined,
        consumptionHistoryType: 'unit',
        announcements: undefined,
        announcementType: 'announcement',
        barcodeShow: false,
        qrCodeShow: false,
    };

    componentDidMount() {
        this.getBilling();
        this.getConsumptionHistory();
        this.getAnnouncements();
    }

    render() {
        const {ca, theme} = this.props;
        const {barcodeShow, qrCodeShow, billing, announcements} = this.state;
        return (
            <ScrollView>
                <View style={styles.container}>
                    {!barcodeShow && !qrCodeShow && this.renderBilling()}
                    {!barcodeShow &&
                        !qrCodeShow &&
                        this.renderConsumptionHistory()}
                    {/* {announcements &&
                        !barcodeShow &&
                        !qrCodeShow &&
                        this.renderAnnouncement()} */}
                    {!barcodeShow && !qrCodeShow && (
                        <View style={{height: 40}}>
                            <PoweredBy theme={theme} />
                        </View>
                    )}
                    {barcodeShow && (
                        <BarcodeView
                            value={ca}
                            hintText={billing.barcodeHint}
                            onClose={this.onBarcodeViewClose}
                        />
                    )}
                    {qrCodeShow && (
                        <QRCodeView
                            qrCodeString={billing.qrCodeString}
                            hintText={billing.qrCodeHint}
                            onClose={this.onQRCodeViewClose}
                        />
                    )}
                </View>
            </ScrollView>
        );
    }

    renderBilling() {
        const {ca, theme} = this.props;
        const {billing} = this.state;
        let isPaid = billing && billing.amount === 0;
        return (
            <View style={styles.billingContainer}>
                <View style={styles.billingBackground}>
                    <FullBillingBG theme={theme} />
                </View>
                <View style={styles.firstContainer}>
                    <Text
                        style={[
                            styles.text,
                            {
                                color:
                                    theme === 'light' ? '#4b4b4b' : '#ffffff',
                            },
                        ]}>
                        บัญชีแสดงสัญญา
                    </Text>
                    <Text
                        style={[
                            styles.text,
                            styles.textCA,
                            {
                                fontWeight: 'bold',
                                color:
                                    theme === 'light' ? '#fe7e34' : '#ffffff',
                            },
                        ]}>
                        {billing && ca ? ca : '-'}
                    </Text>
                </View>
                <View style={styles.secondContainer}>
                    <Text
                        style={[
                            styles.text,
                            styles.textAmount,
                            {
                                color:
                                    theme === 'light' || theme === 'dark'
                                        ? '#fe7e34'
                                        : '#ffffff',
                            },
                        ]}>
                        <Text
                            style={[
                                styles.text,
                                styles.textCurency,
                                {
                                    color:
                                        theme === 'light' || theme === 'dark'
                                            ? '#fe7e34'
                                            : '#ffffff',
                                },
                            ]}>
                            ฿
                        </Text>{' '}
                        {billing ? numberWithCommas(billing.amount, 2) : '-'}
                    </Text>
                    <Text
                        style={[
                            styles.text,
                            {
                                marginTop: -8,
                                textAlign: 'right',
                                color:
                                    theme === 'light' ? '#4b4b4b' : '#ffffff',
                            },
                        ]}>
                        {` วันครบกำหนด ${
                            billing && !isPaid && billing.dueDate
                                ? moment
                                      .utc(billing.dueDate, 'YYYY-MM-DD')
                                      .add(543, 'year')
                                      .format('DD/MM/YY')
                                : '-'
                        }`}
                    </Text>
                </View>
                <View
                    style={[
                        styles.thirdContainer,
                        {
                            backgroundColor:
                                theme === 'light' || theme === 'dark'
                                    ? 'rgba(192, 192, 192, 0.1)'
                                    : 'rgba(0, 0, 0, 0.1)',
                        },
                    ]}>
                    <Text
                        style={[
                            styles.text,
                            {
                                flex: 1,
                                color:
                                    theme === 'light' ? '#4b4b4b' : '#ffffff',
                            },
                        ]}>
                        สแกนเพื่อชำระ
                    </Text>
                    <TouchableOpacity
                        style={{marginLeft: 16}}
                        disabled={!billing || isPaid}
                        onPress={this.onBarcodePress}>
                        <View
                            style={[
                                styles.circleButton,
                                {
                                    backgroundColor:
                                        billing && !isPaid
                                            ? theme === 'light' ||
                                              theme === 'dark'
                                                ? '#fe7e34'
                                                : 'rgba(255, 255, 255, 0.3)'
                                            : 'rgba(255, 255, 255, 0.3)',
                                },
                            ]}>
                            <Image
                                style={styles.circleButtonIcon}
                                source={require('../assets/images/barcode.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{marginLeft: 16}}
                        disabled={!billing || isPaid}
                        onPress={this.onQRCodePress}>
                        <View
                            style={[
                                styles.circleButton,
                                {
                                    backgroundColor:
                                        billing && !isPaid
                                            ? theme === 'light' ||
                                              theme === 'dark'
                                                ? '#fe7e34'
                                                : 'rgba(255, 255, 255, 0.3)'
                                            : 'rgba(255, 255, 255, 0.3)',
                                },
                            ]}>
                            <Image
                                style={styles.circleButtonIcon}
                                source={require('../assets/images/qrcode.png')}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    renderConsumptionHistory() {
        const {ca, theme} = this.props;
        const {consumptionHistory, consumptionHistoryType} = this.state;
        let labels = [];
        let data = [];
        if (consumptionHistory) {
            consumptionHistory.forEach(item => {
                labels.push(months[item.month]);
                data.push(
                    (consumptionHistoryType === 'amount'
                        ? item.amount
                        : item.unit) || null,
                );
            });
        }
        return (
            <View
                style={{
                    backgroundColor: theme === 'dark' ? '#151f28' : '#ffffff',
                    height: 288,
                    alignItems: 'center',
                }}>
                {consumptionHistory && (
                    <>
                        <View
                            style={{
                                height: 40,
                                marginHorizontal: 56,
                                flexDirection: 'row',
                                alignItems: 'center',
                                borderBottomColor: 'rgba(127, 127, 127, 0.38)',
                                borderBottomWidth: 1,
                            }}>
                            {labels.map((item, index) => (
                                <Text
                                    key={index}
                                    style={{
                                        fontFamily: 'DBOzoneX',
                                        fontSize: 22,
                                        flex: 1,
                                        textAlign: 'center',
                                        color:
                                            index === labels.length - 1
                                                ? '#fe7e34'
                                                : 'rgba(127, 127, 127, 0.54)',
                                    }}>
                                    {item}
                                </Text>
                            ))}
                        </View>
                        <VictoryChart
                            width={windowWidth}
                            height={200}
                            padding={{
                                top: 32,
                                bottom: 16,
                                left: 80,
                                right: 80,
                            }}>
                            <Defs>
                                <LinearGradient
                                    id="barChart"
                                    x1="0%"
                                    x2="0%"
                                    y1="100%"
                                    y2="0%">
                                    <Stop offset="0%" stopColor="#c43a31" />
                                    <Stop offset="100%" stopColor="#fbbb5c" />
                                </LinearGradient>
                            </Defs>
                            <VictoryAxis
                                style={{
                                    tickLabels: {
                                        fill:
                                            theme === 'dark'
                                                ? '#151f28'
                                                : '#ffffff',
                                    },
                                }}
                            />
                            <VictoryBar
                                style={{data: {fill: 'url(#barChart)'}}}
                                barRatio={1}
                                data={data}
                                animate={{
                                    onLoad: {duration: 1000},
                                    duration: 0,
                                }}
                            />
                            <VictoryLine
                                style={{
                                    data: {stroke: '#c43a31'},
                                }}
                                data={data}
                                interpolation="natural"
                                animate={{
                                    onLoad: {duration: 1000},
                                    duration: 0,
                                }}
                            />
                            <VictoryScatter
                                style={{
                                    data: {
                                        stroke: '#c43a31',
                                        strokeWidth: 2,
                                        fill: '#ffffff',
                                    },
                                    labels: {
                                        stroke:
                                            theme === 'dark'
                                                ? '#151f28'
                                                : '#ffffff',
                                        strokeWidth: 4,
                                        fontSize: 12,
                                    },
                                }}
                                size={5}
                                data={data}
                                labels={data.map(item =>
                                    numberWithCommas(
                                        item,
                                        consumptionHistoryType === 'amount'
                                            ? 2
                                            : 0,
                                    ),
                                )}
                            />
                            <VictoryScatter
                                style={{
                                    data: {
                                        stroke: '#c43a31',
                                        strokeWidth: 2,
                                        fill: '#ffffff',
                                    },
                                    labels: {
                                        fill: '#c43a31',
                                        fontSize: 12,
                                    },
                                }}
                                size={5}
                                data={data}
                                labels={data.map(item =>
                                    numberWithCommas(
                                        item,
                                        consumptionHistoryType === 'amount'
                                            ? 2
                                            : 0,
                                    ),
                                )}
                            />
                        </VictoryChart>
                        <View
                            style={{
                                height: 36,
                                borderRadius: 18,
                                backgroundColor:
                                    theme === 'dark' ? '#111b24' : '#fe7e34',
                                padding: 4,
                                width: 280,
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}>
                            <TouchableOpacity
                                style={{
                                    height: 28,
                                    borderRadius: 14,
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor:
                                        consumptionHistoryType === 'unit'
                                            ? '#ffffff'
                                            : undefined,
                                }}
                                onPress={() => {
                                    this.setState({
                                        consumptionHistoryType: 'unit',
                                    });
                                }}>
                                <Text
                                    style={{
                                        fontFamily: 'DBOzoneX',
                                        fontSize: 22,
                                        textAlign: 'center',
                                        color:
                                            consumptionHistoryType === 'unit'
                                                ? theme === 'dark'
                                                    ? '#111b24'
                                                    : '#fe7e34'
                                                : '#ffffff',
                                    }}>
                                    หน่วยไฟฟ้า
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{
                                    height: 28,
                                    borderRadius: 14,
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor:
                                        consumptionHistoryType === 'amount'
                                            ? '#ffffff'
                                            : undefined,
                                }}
                                onPress={() => {
                                    this.setState({
                                        consumptionHistoryType: 'amount',
                                    });
                                }}>
                                <Text
                                    style={{
                                        fontFamily: 'DBOzoneX',
                                        fontSize: 22,
                                        textAlign: 'center',
                                        color:
                                            consumptionHistoryType === 'amount'
                                                ? theme === 'dark'
                                                    ? '#111b24'
                                                    : '#fe7e34'
                                                : '#ffffff',
                                    }}>
                                    หน่วยเงิน (บาท)
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </>
                )}
            </View>
        );
    }

    renderAnnouncement() {
        const {theme} = this.props;
        // const {announcements, announcementType} = this.state;
        const announcements = [
            {
                key: 1,
            },
            {
                key: 2,
            },
            {
                key: 3,
            },
            {
                key: 4,
            },
            {
                key: 5,
            },
        ];
        return (
            <View
                style={{
                    backgroundColor: theme === 'dark' ? '#111b24' : '#f8f6f7',
                    height: 240,
                    paddingVertical: 16,
                }}>
                <Paginator
                    data={announcements}
                    renderItem={({item, index}) => (
                        <View
                            style={{
                                backgroundColor:
                                    theme === 'dark' ? '#323d47' : '#ffffff',
                                borderRadius: 8,
                                height: 160,
                                ...(index === 0 ||
                                index === announcements.length - 1
                                    ? {
                                          width: windowWidth - 104,
                                          ...(index === 0
                                              ? {
                                                    marginLeft: 16,
                                                    marginRight: 8,
                                                }
                                              : {
                                                    marginLeft: 8,
                                                    marginRight: 16,
                                                }),
                                      }
                                    : {
                                          marginHorizontal: 8,
                                          width: windowWidth - 96,
                                      }),
                            }}>
                            <Text>{item.key}</Text>
                        </View>
                    )}
                    keyExtractor={item => item.key.toString()}
                    itemWidth={windowWidth - 80}
                />
            </View>
        );
    }

    onBarcodePress = () => {
        this.setState({
            barcodeShow: true,
        });
    };

    onBarcodeViewClose = () => {
        this.setState({
            barcodeShow: false,
        });
    };

    onQRCodePress = () => {
        this.setState({
            qrCodeShow: true,
        });
    };

    onQRCodeViewClose = () => {
        this.setState({
            qrCodeShow: false,
        });
    };

    getBilling() {
        const {env, apiKey, clientCode, ca} = this.props;
        fetchBilling({
            env,
            apiKey,
            clientCode,
            ca,
        })
            .then(data => {
                this.setState({
                    billing: data,
                });
            })
            .catch(error => {
                try {
                    if (this.onErrorCalled === false) {
                        this.props.onError(error);
                        this.onErrorCalled = true;
                    }
                } catch (error) {}
            });
    }

    getConsumptionHistory() {
        const {env, apiKey, clientCode, ca} = this.props;
        fetchConsumptionHistory({
            env,
            apiKey,
            clientCode,
            ca,
            type: 'all',
        })
            .then(data => {
                this.setState({
                    consumptionHistory: data,
                });
            })
            .catch(error => {
                try {
                    if (this.onErrorCalled === false) {
                        this.props.onError(error);
                        this.onErrorCalled = true;
                    }
                } catch (error) {}
            });
    }

    async getAnnouncements() {
        const {env, apiKey, clientCode, ca} = this.props;

        this.setState({
            announcements: [],
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    billingContainer: {
        position: 'relative',
        height: 240,
    },
    billingBackground: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        transform: [
            {
                translateX: -(
                    (windowWidth <= 480
                        ? 480 - windowWidth
                        : windowWidth - 480) / 2
                ),
            },
        ],
    },
    text: {
        color: 'white',
        fontFamily: 'DBOzoneX',
        fontSize: 22,
    },
    textCA: {
        fontWeight: 'bold',
        fontSize: 32,
    },
    textAmount: {
        fontWeight: 'bold',
        fontSize: 56,
        textAlign: 'right',
    },
    textCurency: {
        fontWeight: 'bold',
        fontSize: 40,
    },
    firstContainer: {
        flex: 1,
        padding: 24,
        paddingBottom: 0,
    },
    secondContainer: {
        flex: 2,
        padding: 24,
        paddingTop: 12,
    },
    thirdContainer: {
        flex: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 24,
    },
    circleButton: {
        width: 52,
        height: 52,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleButtonIcon: {
        width: 26,
        height: 26,
    },
});
