import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import Billing from './common/Billing'
import ComponentBackground from './common/ComponentBackground'
import { withDimension, defineThemeValue } from './common/utils'
import PoweredBy from './common/PoweredBy'

let MEAEnergyBilling = ({ ca, theme, onError, dimension }) => {
  const define = defineThemeValue(theme)
  const backgroundColor = define('#343F4A', '#FFFFFF', '#EB670C')
  const secondBackgroundColor = define('rgba(255,255,255,0.03)', '#FCF6EE', 'rgba(245,202,29,0.18)')
  const scanBGColor = define('#26313B', 'transparent', '#F7F7F7')
  return (
    <View style={[styles.container, { backgroundColor }]}>
      <View style={styles.componentBackground}>
        <ComponentBackground theme={theme} {...dimension} radius={15} secondBackgroundColor={secondBackgroundColor} />
      </View>
      <Billing
        ca={ca}
        theme={theme}
        onError={onError}
        scanBGColorProp={scanBGColor}
        colorBottom={theme === 'mea'}
        hideSendOutage
      />
      <View style={{ height: 40 }}>
        <PoweredBy
          overrideBgColor={theme === 'dark' ? 'transparent' : scanBGColor}
          right
          theme={theme === 'mea' ? 'light' : theme}
          bottomRadius={15}
        />
      </View>
    </View>
  )
}

MEAEnergyBilling = withDimension(MEAEnergyBilling)
export { MEAEnergyBilling }

const styles = StyleSheet.create({
  container: {
    height: 290,
    paddingTop: 10,
    borderRadius: 15,
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  title: {
    paddingLeft: 20,
    fontFamily: 'DBOzoneX',
    fontSize: 20,
    color: '#817F7F',
    paddingBottom: 10,
  },
  componentBackground: {
    borderRadius: 15,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
})
