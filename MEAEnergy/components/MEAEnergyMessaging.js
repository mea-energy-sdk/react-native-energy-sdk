import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import Announments from './common/announcement/Announcements'
import ComponentBackground from './common/ComponentBackground'
import { withDimension, defineThemeValue } from './common/utils'
import PoweredBy from './common/PoweredBy'

let MEAEnergyMessaging = ({ ca, theme, onError, dimension }) => {
  const define = defineThemeValue(theme)
  const backgroundColor = define('#343F4A', '#FFFFFF', '#EB670C')
  const secondBackgroundColor = define('rgba(255,255,255,0.03)', '#FCF6EE', 'rgba(245,202,29,0.18)')
  const titleColor = define('#FFFFFF', '#817F7F', '#FFFFFF')
  const messageBackgroundColor = define('#424F5C', '#ffffff', '#ffffff')
  return (
    <View style={[styles.container, { backgroundColor }]}>
      <View style={styles.componentBackground}>
        <ComponentBackground theme={theme} {...dimension} radius={15} secondBackgroundColor={secondBackgroundColor} />
      </View>
      <Text style={{ ...styles.title, color: titleColor }}>ข้อความแจ้งเตือนจากการไฟฟ้านครหลวง</Text>
      <Announments
        ca={ca}
        theme={theme}
        onError={onError}
        messageBackgroundColor={messageBackgroundColor}
        containerBackgroundColor={theme === 'mea' ? '#F7F7F7' : undefined}
      />
      <View style={{ height: 40 }}>
        <PoweredBy
          right
          overrideBgColor="transparent"
          theme={theme === 'mea' ? 'dark' : theme}
          white={theme === 'mea'}
        />
      </View>
    </View>
  )
}

MEAEnergyMessaging = withDimension(MEAEnergyMessaging)
export { MEAEnergyMessaging }

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    borderRadius: 15,
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  title: {
    paddingLeft: 20,
    fontFamily: 'DBOzoneX',
    fontSize: 20,
    color: '#817F7F',
    paddingBottom: 10,
    fontWeight: '700',
  },
  componentBackground: {
    borderRadius: 15,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
})
