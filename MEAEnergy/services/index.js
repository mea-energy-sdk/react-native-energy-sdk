import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';

const ENV = {
    QA: 'qa',
    STAGING: 'staging',
    PRODUCTION: 'production',
};
let defaultEnv = ENV.staging;
let defaultApiKey;
let defaultClientCode;

const bundleId = DeviceInfo.getBundleId();

const serviceUrl = {
    qa: {
        registerCaUrl:
            'https://mea-api.stage.mapboss.co.th/energy/register/register-ca',
        fetchBillingUrl:
            'https://mea-api.stage.mapboss.co.th/energy/billing/enquiry',
        fetchConsumptionHistoryUrl:
            'https://mea-api.stage.mapboss.co.th/energy/consumption-history/history',
    },
    staging: {
        registerCaUrl: 'https://apidev.mea.or.th/energy/register/register-ca',
        fetchBillingUrl: 'https://apidev.mea.or.th/energy/billing/enquiry',
        fetchConsumptionHistoryUrl:
            'https://apidev.mea.or.th/energy/consumption-history/history',
    },
    production: {
        registerCaUrl: 'https://api.mea.or.th/energy/register/register-ca',
        fetchBillingUrl: 'https://api.mea.or.th/energy/billing/enquiry',
        fetchConsumptionHistoryUrl:
            'https://api.mea.or.th/energy/consumption-history/history',
    },
};

const setEnv = env => {
    if (env === undefined || env === null) {
        defaultEnv = undefined;
    } else if (serviceUrl[env]) {
        defaultEnv = env;
    } else {
        throw new Error(`Environment ${env} not found.`);
    }
};

const setApiKey = apiKey => {
    defaultApiKey = apiKey;
};

const setClientCode = clientCode => {
    defaultClientCode = clientCode;
};

const getRequestHeaders = function(apiKey, clientCode, headers = {}) {
    return {
        ...headers,
        'X-Api-Key': apiKey,
        'X-Client-Code': clientCode,
        'X-Sdk-Version': '1.0.1',
        ...(Platform.OS === 'ios'
            ? {
                  'X-Ios-Bundle-Identifier': bundleId,
                  'X-Sdk-Platform': 'react-native-ios',
              }
            : Platform.OS === 'android'
            ? {
                  'X-Android-Package': bundleId,
                  'X-Sdk-Platform': 'react-native-android',
              }
            : {}),
    };
};

const registerCa = (options = {}) => {
    return new Promise(async (resolve, reject) => {
        try {
            const {
                env = defaultEnv,
                apiKey = defaultApiKey,
                clientCode = defaultClientCode,
                ca,
                projectCode,
                projectName,
                projectNameEn,
                projectAddress,
                projectAddressEn,
            } = options;
            const response = await fetch(serviceUrl[env].registerCaUrl, {
                method: 'PUT',
                headers: getRequestHeaders(apiKey, clientCode, {
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    ca,
                    projectCode,
                    projectName,
                    projectNameEn,
                    projectAddress,
                    projectAddressEn,
                }),
            });
            const responseJson = await response.json();
            if (responseJson.errors) {
                reject(responseJson.errors);
            } else {
                resolve(responseJson.data);
            }
        } catch (error) {
            reject([
                {
                    title: 'CLIENT_ERROR',
                    detail: error,
                },
            ]);
        }
    });
};

const fetchBilling = (options = {}) => {
    return new Promise(async (resolve, reject) => {
        try {
            const {
                env = defaultEnv,
                apiKey = defaultApiKey,
                clientCode = defaultClientCode,
                ca,
            } = options;
            const response = await fetch(
                `${serviceUrl[env].fetchBillingUrl}?requireQRCodeString=true&requireQRCodeHint=true&requireBarcodeHint=true&ca=${ca}`,
                {
                    headers: getRequestHeaders(apiKey, clientCode),
                },
            );
            const responseJson = await response.json();
            if (responseJson.errors) {
                reject(responseJson.errors);
            } else {
                resolve(responseJson.data);
            }
        } catch (error) {
            reject([
                {
                    title: 'CLIENT_ERROR',
                    detail: error,
                },
            ]);
        }
    });
};

const fetchConsumptionHistory = (options = {}) => {
    return new Promise(async (resolve, reject) => {
        try {
            const {
                env = defaultEnv,
                apiKey = defaultApiKey,
                clientCode = defaultClientCode,
                ca,
                type,
            } = options;
            const response = await fetch(
                `${serviceUrl[env].fetchConsumptionHistoryUrl}?type=${type}&ca=${ca}`,
                {
                    headers: getRequestHeaders(apiKey, clientCode),
                },
            );
            const responseJson = await response.json();
            if (responseJson.errors) {
                reject(responseJson.errors);
            } else {
                resolve(responseJson.data);
            }
        } catch (error) {
            reject([
                {
                    title: 'CLIENT_ERROR',
                    detail: error,
                },
            ]);
        }
    });
};

module.exports = {
    ENV,
    setEnv,
    setApiKey,
    setClientCode,
    registerCa,
    fetchBilling,
    fetchConsumptionHistory,
};
