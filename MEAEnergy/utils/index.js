export const numberWithCommas = function(number, numOfDigit = 3) {
  if (typeof number === 'number' || typeof number === 'string') {
    if (numOfDigit) {
      return Number(number)
        .toFixed(numOfDigit)
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    } else {
      return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
  } else {
    return '';
  }
};

export const validateEmailFormat = value => {
  return !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? false
    : true;
};

export const toThaiDigit = text => {
  var id = ['๐', '๑', '๒', '๓', '๔', '๕', '๖', '๗', '๘', '๙'];
  return text.replace(/[0-9]/g, function(w) {
    return id[Number(w)];
  });
};
